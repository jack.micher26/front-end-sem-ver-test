# BNM FrontEnd

## Purpose

The PlaceOrder FrontEnd currently utilizes React App in order to provide a dynamic UI that allows users to select items, provide personal preferences and allergies, and ultimately place orders to the BNM food pantry. This is accomplished through utilizing api calls to connect to the PlaceOrder Backend and store the orders within the mongo database.

## Current status

Currently the project is in the process of being transferred over from the current React App to Vue. Vue is an open-source framework for building user interfaces and allows for html, JavaScript, and css styling to be contained within the same single file components. Once this has been completed new installation instructions will be added this page.

## Learn More

- If you would like to learn more about the Vue framework you can check out their extensive documentation and tutorials here: https://vuejs.org/guide/introduction.html
- If you would like to learn more about React APP currently used in this project check out these links:
  - https://facebook.github.io/create-react-app/docs/getting-started
  - https://reactjs.org/

### LibreFoodPantry Vue Example

- If you would like to check out a simple example of how Vue is being utilized within this specific project check out this link: https://gitlab.com/LibreFoodPantry/training/microservices/microservices-examples/frontend


## Running the FrontEnd

# Clone this project
git clone https://gitlab.com/LibreFoodPantry/client-solutions/bear-necessities-market/ordersystem/placeorder/placeordermodule.git

# Access
cd placeordermodule

# Run the project
docker-compose up

# The Frontend server will initialize in the <http://localhost:3000>
# The Frontend server will initialize in the <http://localhost:4000/api>
