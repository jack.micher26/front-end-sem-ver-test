$(document).ready(function() {
    // getting the order type
    // Does this return a Golden Bear Order if Custom Order is not true? Should we consider what the default value should be instead, or just reconsider the order types?
    function getOrderType(){
        if(($('input[id="customRadio"]:checked').val()) === 'on'){
            return "Custom Order";
        }
        return "Golden Bear Order";
    }
    // Getting the Contents of the order
    function getItemsOrdered(){
        var itemsOrdered = "" ;
        if(($('input[id="cerealCheckbox"]:checked').val()) === 'on'){
            itemsOrdered = itemsOrdered.concat("Cereal, ");
        }
        if(($('input[id="pastaCheckbox"]:checked').val()) === 'on'){
            itemsOrdered = itemsOrdered.concat("Pasta, ");
        }
        if(($('input[id="pbCheckbox"]:checked').val()) === 'on'){
            itemsOrdered = itemsOrdered.concat("Peanut Butter, ");
        }
        if(($('input[id="tunaCheckbox"]:checked').val()) === 'on'){
            itemsOrdered = itemsOrdered.concat("Tuna, ");
        }
        if(($('input[id="soupCheckbox"]:checked').val()) === 'on'){
            itemsOrdered = itemsOrdered.concat("Soup, ");
        }
        if(($('input[id="vegCheckbox"]:checked').val()) === 'on'){
            itemsOrdered = itemsOrdered.concat("Canned Vegetables, ");
        }
        if(($('input[id="fruitCheckbox"]:checked').val()) === 'on'){
            itemsOrdered = itemsOrdered.concat("Canned Fruit, ");
        }
        return itemsOrdered;
    }

    //when the submit button is pressed
    $("#submitButton").click(function() {
        //Creating post request for submiting a new order
        axios.post('http://localhost:4000/api/orders/post', {
            forderType: getOrderType(),
            itemsOrdered: getItemsOrdered(),
            dietaryRestrictions: $("#restrictions").val(),
            commentsPreferences: $("#preferences").val(),
            email: $("#e-mail").val(),
            emailVerified: true
        })
            .then(function (response) {
                console.log(response);
            })
            .catch(function (error) {
                console.log(error);
            });
    });
});
